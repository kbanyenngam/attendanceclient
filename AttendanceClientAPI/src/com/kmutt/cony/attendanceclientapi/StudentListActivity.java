package com.kmutt.cony.attendanceclientapi;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kmutt.cony.api.AttendanceAPIZombie;
import com.kmutt.cony.model.zombie.StudentAttendance;
import com.kmutt.cony.model.zombie.StudentAttendanceEntry;
import com.kmutt.cony.model.zombie.User;
import com.squareup.picasso.Picasso;

@SuppressLint("InflateParams")
public class StudentListActivity extends BaseActivity {
	private ListView mListView;
	private AttendanceAPIZombie mAttendanceAPI;
	private List<StudentAttendance> mListStudent;
	public ProgressDialog mProgress;
	private ScheduleAdapter mScheduleAdapter;
	private int mGroupId;
	private int mClassId;
	private ImageView mIvEditMode;
	private ImageView mBtAbsent;
	private ImageView mBtLate;
	private ImageView mBtPresent;

	private boolean isEditMode;
	private int mEditing_status[];
	private int mSelected_Mode;
	
	private ProgressDialog mProgressDialog;
	
	private List<StudentAttendanceEntry> mEntryList ;
	private ImageView mBackgroup;
	private TextView mTvTitle;
	private TextView mTvStat_present;
	private TextView mTvStat_late;
	private TextView mTvStat_absent;
	private TextView mTvTotalStudent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mEntryList = new ArrayList<StudentAttendanceEntry>();
		
		FrameLayout parentContent = (FrameLayout) findViewById(R.id.parent_content);
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.lb_title_student_list);
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View view = inflater.inflate(R.layout.activity_student_list, null);
		parentContent.addView(view);
		
		mBackgroup = (ImageView) findViewById(R.id.backgroup);
		mTvTitle = (TextView) findViewById(R.id.tv_course_name);
		mTvStat_present = (TextView) findViewById(R.id.tv_stat_present);
		mTvStat_late = (TextView) findViewById(R.id.tv_stat_late);
		mTvStat_absent = (TextView) findViewById(R.id.tv_stat_absent);
		mTvTotalStudent= (TextView) findViewById(R.id.tv_total_student);
		
		mIvEditMode = (ImageView) findViewById(R.id.iv_edit);
		mIvEditMode.setOnClickListener(mClickedEdit);

		mBtAbsent = (ImageView) findViewById(R.id.bt_red);
		mBtLate = (ImageView) findViewById(R.id.bt_yellow);
		mBtPresent = (ImageView) findViewById(R.id.bt_green);

		mBtAbsent.setOnClickListener(mCheckInState);
		mBtLate.setOnClickListener(mCheckInState);
		mBtPresent.setOnClickListener(mCheckInState);

		mListView = (ListView) findViewById(R.id.lv_student_list);
		mScheduleAdapter = new ScheduleAdapter(this);
		mListView.setAdapter(mScheduleAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				StudentAttendance studentAttendance = (StudentAttendance) parent
						.getItemAtPosition(position);
				int studenID = studentAttendance.getStudent().getUserId();
				Intent intentStudentInfo = new Intent(getApplicationContext(),
						StudentInfoActivity.class);
				intentStudentInfo.putExtra("id", studenID);
				intentStudentInfo.putExtra("group", mGroupId);
				startActivityForResult(intentStudentInfo, 1);
			}
		});
		try {
			mAttendanceAPI = AttendanceAPIZombie.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Intent intent = getIntent();
		if (intent != null) {

			mGroupId = intent.getIntExtra("group", -1);
			mClassId = intent.getIntExtra("classid", -1);
			String courseName = intent.getStringExtra("courseName");
			String date = intent.getStringExtra("date");
			
			//set ui
			mTvTitle.setText(courseName+" "+ date);
		}
		
		
		new ScheduleTask().execute(true);

	}

	private OnClickListener mCheckInState = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isEditMode) {

				if (v.findViewById(R.id.bt_green) != null) {
					mSelected_Mode = StudentAttendance.STATUS_PRESENT;
				} else if (v.findViewById(R.id.bt_yellow) != null) {
					mSelected_Mode = StudentAttendance.STATUS_LATE;
				} else {
					mSelected_Mode = StudentAttendance.STATUS_ABSENCE;
				}
				setInActiveViewEditMode();
				v.setSelected(true);
			}

		}
	};
	@SuppressLint("NewApi")
	private OnClickListener mClickedEdit = new OnClickListener() {

		@Override
		public void onClick(View v) {
			ImageView imageview = (ImageView) v;
			if (!isEditMode) {
				setInActiveViewEditMode();

				mBtPresent.setSelected(false);
				mBtPresent.setAlpha(1f);
				mSelected_Mode = StudentAttendance.STATUS_PRESENT;

				isEditMode = true;
				// clear mEditing_status
				if (mEditing_status != null) {
					for (int i = 0; i < mEditing_status.length; i++) {
						mEditing_status[i] = -1;
					}
				}
				imageview.setSelected(true);
			} else {
				isEditMode = false;
				setActiveViewEditMode();
				mScheduleAdapter.notifyDataSetChanged();
				imageview.setSelected(false);
				
				List<StudentAttendance> list =mScheduleAdapter.getList();
				for(int i=0; i<mEditing_status.length;i++){
					if(mEditing_status[i]!=-1){
						int _id = list.get(i).getStudent().getUserId();
						StudentAttendanceEntry entry = new StudentAttendanceEntry();
						entry.setUserId(_id);
						entry.setClassId(mClassId);
						entry.setStatus(mEditing_status[i]);
						mEntryList.add(entry);
					}
				}
				new SendStatusTask().execute();

			}

		}
	};

	private void setInActiveViewEditMode() {
		mBtAbsent.setSelected(false);
		mBtLate.setSelected(false);
		mBtPresent.setSelected(false);

	}

	private void setActiveViewEditMode() {
		mBtAbsent.setSelected(true);
		mBtLate.setSelected(true);
		mBtPresent.setSelected(true);


	}
	
	private class SendStatusTask extends AsyncTask<Boolean, String, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(StudentListActivity.this);
			mProgressDialog.setMessage("Checking in ...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}
		@Override
		protected Boolean doInBackground(Boolean... params) {
			
			try {
				mAttendanceAPI.updateStudentCheckInStatus(mEntryList,true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			new ScheduleTask().execute(false);
		}
		
	}

	private class ScheduleTask extends AsyncTask<Boolean, String, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgress = new ProgressDialog(StudentListActivity.this);
			mProgress.setMessage("Getting Data ...");
			mProgress.setIndeterminate(false);
			mProgress.setCancelable(false);
			mProgress.show();
		}

		@Override
		protected Boolean doInBackground(Boolean... params) {
			boolean result = false;
			try {
				Boolean isCache = params[0];  
				mListStudent = mAttendanceAPI.getClassScheduleCheckIn(mClassId,isCache);
				result = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgress.dismiss();
			if (result) {
				// update view
				mTvTotalStudent.setText("Total Students "+mListStudent.size());
				
				mScheduleAdapter.updateSchedule(mListStudent);
				int present = 0;
				int late = 0;
				int absent = 0;
				for(int i=0;i<mListStudent.size();i++){
					StudentAttendance studentAttendant = mListStudent.get(i);
					int status = studentAttendant.getCheckInStatus();
					if(status == StudentAttendance.STATUS_PRESENT){
						present++;
					}else if(status == StudentAttendance.STATUS_LATE){
						late++;
					}else if(status == StudentAttendance.STATUS_ABSENCE){
						absent++;
					}
				}
				
				setStatView(present, late, absent);
				mScheduleAdapter.notifyDataSetChanged();
				
				
			} else {
				// handle:something went wrong
			}
		}

		private void setStatView(int present,int late,int absent) {
			String Present = String.format("Present %s", present);
			mTvStat_present.setText(Present);
			String Late = String.format("Late %s", late);
			mTvStat_late.setText(Late);
			String Absence = String.format("Absence %s", absent);
			mTvStat_absent.setText(Absence);
		}
	}

	class ScheduleAdapter extends BaseAdapter {
		List<StudentAttendance> iListStudentStat;

		private Context mContext;

		public ScheduleAdapter(Context context) {
			mContext = context;
			iListStudentStat = new ArrayList<StudentAttendance>();
		}

		public void updateSchedule(List<StudentAttendance> listSchedul) {
			iListStudentStat.clear();
			iListStudentStat.addAll(listSchedul);
			mEditing_status = new int[listSchedul.size()];
		}
		public List<StudentAttendance> getList(){
			return iListStudentStat;
		}
		@Override
		public int getCount() {
			return iListStudentStat.size();
		}

		@Override
		public StudentAttendance getItem(int position) {
			return iListStudentStat.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			StudentAttendance studentAttendant = iListStudentStat.get(position);
			if (convertView == null) {
				// inflate item
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.item_lv_student,
						parent, false);
				// new holder
				holder = new ViewHolder();
				holder.tvName = (TextView) convertView
						.findViewById(R.id.tv_subject_code);
				holder.ivStatus = (ImageView) convertView
						.findViewById(R.id.iv_status);
				holder.ivProfile = (ImageView)convertView
						.findViewById(R.id.iv_profile);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			int status=StudentAttendance.STATUS_NA;
			if (studentAttendant != null) {
				User user = studentAttendant.getStudent();
				status = studentAttendant.getCheckInStatus();

				String information = String.format(user.getFirstName() + " "
						+ user.getLastName() + "\n"+user.getPredefinedId());
				holder.tvName.setText(information);
				
				String colorStr = null;
				int resBg = -1;
				if (status == StudentAttendance.STATUS_PRESENT) {
					colorStr = mContext.getResources().getString(
							R.color.green);
					resBg = R.drawable.balloon_clean_green;
					convertView.setBackgroundResource(resBg);
				} else if (status == StudentAttendance.STATUS_LATE) {
					colorStr = mContext.getResources().getString(
							R.color.yellow);
					resBg = R.drawable.balloon_clean_yellow;
					convertView.setBackgroundResource(resBg);
				} else if(status == StudentAttendance.STATUS_ABSENCE){
					colorStr = mContext.getResources().getString(
							R.color.red);
					resBg = R.drawable.balloon_clean_red;
					convertView.setBackgroundResource(resBg);
				}else{
					colorStr = mContext.getResources().getString(
							android.R.color.black);
					convertView.setBackgroundResource(R.drawable.balloon_clean_na);
				}
				holder.ivStatus.setBackgroundColor(Color.parseColor(colorStr));
				holder.tvName.setTextColor(Color.parseColor(colorStr));
				String urlImage = studentAttendant.getStudent().getPhoto();
				if(urlImage!=null){
					Picasso.with(getApplicationContext()).load(urlImage)
					.error(R.drawable.ic_action_user)
					.into(holder.ivProfile);
				}
				
			}

			holder.tvName.setTag(position);
			holder.ivStatus.setTag(status);
			

			convertView.setOnClickListener(clickItem);
			
			return convertView;
		}

		private OnClickListener clickItem = new OnClickListener() {

			@Override
			public void onClick(View v) {
				int position = ((Integer) v.findViewById(R.id.tv_subject_code)
						.getTag()).intValue();
				int statusBefore = ((Integer) v.findViewById(R.id.iv_status)
						.getTag()).intValue();
				int resBg = -1;
				String colorStr = mContext.getResources().getString(
						android.R.color.black);

				if (isEditMode) {
					if (mEditing_status[position] != mSelected_Mode) {
						mEditing_status[position] = mSelected_Mode;
						// change color
						
						if (mSelected_Mode != StudentAttendance.STATUS_NA) {
							setToggleStatusView(mSelected_Mode,v,resBg,colorStr);
						}else{
							// reset old status
							if(statusBefore == StudentAttendance.STATUS_NA){
								v.setBackgroundColor(Color.TRANSPARENT);
								colorStr = mContext.getResources().getString(
										android.R.color.black);
							}else{
								//TODO hold lastest status
								//setToggleStatusView(statusBefore,v,resBg,colorStr);
								
								
							}
						}
						
					} else {
						
						if(statusBefore == StudentAttendance.STATUS_NA){
							v.setBackgroundResource(R.drawable.balloon_clean_na);
							colorStr = mContext.getResources().getString(
									android.R.color.black);
							mEditing_status[position] = -1;
							TextView tvName = (TextView) v.findViewById(R.id.tv_subject_code);
							tvName.setTextColor(Color.parseColor(colorStr));
						}else{
							
							//setToggleStatusView(statusBefore,v,resBg,colorStr);
						}

					}

				} else {
					StudentAttendance studentAttendance = iListStudentStat
							.get(position);
					int studenID = studentAttendance.getStudent().getUserId();
					Intent intentStudentInfo = new Intent(
							getApplicationContext(), StudentInfoActivity.class);
					intentStudentInfo.putExtra("id", studenID);
					intentStudentInfo.putExtra("group", mGroupId);
					startActivityForResult(intentStudentInfo,1);
				}
				
				

			}

			private void setToggleStatusView(int stat,View v,int resBg,String colorStr) {
				TextView tvName = (TextView) v.findViewById(R.id.tv_subject_code);
				if (stat == StudentAttendance.STATUS_PRESENT) {
					resBg = R.drawable.balloon_clean_green;
					colorStr = mContext.getResources().getString(
							R.color.green);
					v.setBackgroundResource(resBg);
				} else if (stat == StudentAttendance.STATUS_LATE) {
					resBg = R.drawable.balloon_clean_yellow;
					colorStr = mContext.getResources().getString(
							R.color.yellow);
					v.setBackgroundResource(resBg);
				} else if(stat == StudentAttendance.STATUS_ABSENCE){
					resBg = R.drawable.balloon_clean_red;
					colorStr = mContext.getResources().getString(
							R.color.red);
					v.setBackgroundResource(resBg);
				}
				
				tvName.setTextColor(Color.parseColor(colorStr));
				
			}
		};

		class ViewHolder {
			public TextView tvName;
			public ImageView ivStatus;
			public ImageView ivProfile;
		}

	}

	@Override
	public void doRefreshApi() {
		Boolean isCache = false;
		new ScheduleTask().execute(isCache);
	}

}
