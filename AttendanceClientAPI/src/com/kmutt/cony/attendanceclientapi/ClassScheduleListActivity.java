package com.kmutt.cony.attendanceclientapi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kmutt.cony.api.AttendanceAPIZombie;
import com.kmutt.cony.model.zombie.Class;

@SuppressLint("InflateParams") public class ClassScheduleListActivity extends BaseActivity {
	private ListView mListView;
	private List<Class> mListSchedul;
	private ScheduleAdapter mScheduleAdapter;
	private ScheduleTask mScheduleTask;
	private AttendanceAPIZombie mAttendanceAPI;
	private int mGroupID;
	private String mCourseName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FrameLayout parentContent = (FrameLayout) findViewById(R.id.parent_content);
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.lb_title_class_schedule);
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View view = inflater.inflate(R.layout.activity_schedule, null);
		parentContent.addView(view);
		
		mGroupID = getIntent().getIntExtra("group",-1);
		mCourseName = getIntent().getStringExtra("courseName");
		mListSchedul = new ArrayList<Class>();
		
		mScheduleAdapter = new ScheduleAdapter(ClassScheduleListActivity.this);
		mListView = (ListView) findViewById(R.id.lv_schedule);
		mListView.setAdapter(mScheduleAdapter);
		
		//load data
		mAttendanceAPI = AttendanceAPIZombie.getInstance();
		mScheduleTask = new ScheduleTask();
		mScheduleTask.execute(true);
		
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intentStudent = new Intent(getApplicationContext(), StudentListActivity.class);
				Class clazz = (Class) parent.getItemAtPosition(position);
				
				SimpleDateFormat date = new SimpleDateFormat("dd MMM yy HH:mm");
				String dateTime  =date.format(new Date(1000*clazz.getDatetimeSeconds()));
				
				intentStudent.putExtra("group", mGroupID);
				intentStudent.putExtra("classid", clazz.getClassId());
				intentStudent.putExtra("date", dateTime);
				intentStudent.putExtra("courseName", mCourseName);
				
				startActivityForResult(intentStudent,1);
			}
		});
	}
	

	private class ScheduleTask extends AsyncTask<Boolean, String, Boolean> {

		private ProgressDialog mProgress;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgress = new ProgressDialog(ClassScheduleListActivity.this);
			mProgress.setMessage("Getting Data ...");
			mProgress.setIndeterminate(false);
			mProgress.setCancelable(false);
			mProgress.show();
		}

		@Override
		protected Boolean doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			boolean result = false;
			try {
				mListSchedul = mAttendanceAPI.getClassSchedule(mGroupID,params[0]);
				if (mListSchedul != null) {
					result = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			mProgress.dismiss();
			if (result) {
				// update view
				updateListSchedule(mListSchedul);
			} else {
				// handle:something went wrong
			}
		}
	}

	public void updateListSchedule(List<Class> mListSchedul) {
		if(mListSchedul != null && mListSchedul.size()>0) {
			mScheduleAdapter.updateSchedule(mListSchedul);
			mScheduleAdapter.notifyDataSetChanged();
		}
	}

	@SuppressLint("SimpleDateFormat") class ScheduleAdapter extends BaseAdapter {
		private Context mContext;
		private List<Class> mlistSchedul;
		public ScheduleAdapter(Context context) {
			mContext = context;
			mlistSchedul = new ArrayList<Class>();
		}

		public void updateSchedule(List<Class> listSchedul) {
			mlistSchedul.clear();
			mlistSchedul.addAll(listSchedul);
		}

		@Override
		public int getCount() {
			return mlistSchedul.size();
		}

		@Override
		public Class getItem(int position) {
			return mlistSchedul.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				//inflate item
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.item_lv_class,
						parent, false);
				//new holder
				holder = new ViewHolder();
				holder.tvSubject = (TextView) convertView
						.findViewById(R.id.tv_subject_code);
				holder.tvRoom = (TextView) convertView
						.findViewById(R.id.tv_room);
				//set holder in view
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Class classSchedule = mlistSchedul.get(position);
			if(classSchedule != null){
				SimpleDateFormat date = new SimpleDateFormat("dd MMM yy HH:mm");
				String dateTime  =date.format(new Date(1000*classSchedule.getDatetimeSeconds()));
				
				holder.tvRoom.setText("Room "+classSchedule.getClassRoom());
				holder.tvSubject.setText(dateTime+" "+classSchedule.getPeriod()+" hours");
			}
			
			return convertView;
		}

	}

	class ViewHolder {
		public TextView tvRoom;
		public TextView tvSubject;
	}

	@Override
	public void doRefreshApi() {
		mScheduleTask = new ScheduleTask();
		mScheduleTask.execute(false);
	}
	
}
