package com.kmutt.cony.attendanceclientapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kmutt.cony.api.AttendanceAPIZombie;
import com.kmutt.cony.utils.Constants;
import com.kmutt.cony.utils.SharedPrefs;

public abstract class BaseActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//check login
		checkLogin();
		setContentView(R.layout.activity_base);
		findViewById(R.id.iv_refresh).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				doRefreshApi();
			}
		});
		
		findViewById(R.id.bt_logout).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				doLogout();		
				
			}
		});
	}
	private void checkLogin(){
		String activity_name = getClass().getSimpleName();
		if(!activity_name.contains("Login")){
			boolean LoggedIn =AttendanceAPIZombie.getInstance().isLogin();
			if(!LoggedIn){
				doLogout();
			}
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == 2){
			setResult(2);
			finish();
		}
	}
	public void doLogout(){
		// clear all activity
		SharedPrefs.setPreference(getApplicationContext(), Constants.TAG_USER, "");
		SharedPrefs.setPreference(getApplicationContext(), Constants.TAG_PASS, "");
		//clear credential
		AttendanceAPIZombie.getInstance().logout();
		Intent login = new Intent(getApplicationContext(),LoginActivity.class);
		login.putExtra("logout", true);
		startActivity(login);
		setResult(2);
		finish();
	}
	public abstract void doRefreshApi();
}
