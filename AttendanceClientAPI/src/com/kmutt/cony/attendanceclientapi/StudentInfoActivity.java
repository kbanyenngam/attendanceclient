package com.kmutt.cony.attendanceclientapi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kmutt.cony.api.AttendanceAPIZombie;
import com.kmutt.cony.model.zombie.Attendance;
import com.kmutt.cony.model.zombie.StudentAttendance;
import com.kmutt.cony.model.zombie.StudentInfo;
import com.kmutt.cony.model.zombie.User;
import com.squareup.picasso.Picasso;

public class StudentInfoActivity extends BaseActivity{
	private ScheduleAdapter mScheduleAdapter;
	private AttendanceAPIZombie mAttendanceAPI;
	private ListView mListView;
	private TextView mTvName;
	private int mStudentId;
	private int mGroupId;
	private ImageView mIvProfile;
	private ImageView mIvSex;
	private TextView mTvPredefine;
	private TextView mTvStat_present;
	private TextView mTvStat_late;
	private TextView mTvStat_absent;
	
	@SuppressLint("InflateParams") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FrameLayout parentContent = (FrameLayout) findViewById(R.id.parent_content);
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.lb_title_student_info);
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View view = inflater.inflate(R.layout.activity_student_info, null);
		parentContent.addView(view);
		
		mListView = (ListView) findViewById(R.id.lv_student_info);
		mTvName = (TextView) findViewById(R.id.tv_name);
		mTvStat_present = (TextView) findViewById(R.id.tv_stat_present);
		mTvStat_late = (TextView) findViewById(R.id.tv_stat_late);
		mTvStat_absent = (TextView) findViewById(R.id.tv_stat_absent);
		mIvProfile = (ImageView) findViewById(R.id.img_student_info);
		mIvSex = (ImageView) findViewById(R.id.iv_sex);
		mTvPredefine = (TextView) findViewById(R.id.tv_email);
		
		mScheduleAdapter = new ScheduleAdapter(StudentInfoActivity.this);
		mListView.setAdapter(mScheduleAdapter);
		
		try {
			
			mAttendanceAPI = AttendanceAPIZombie.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Intent intent = getIntent();
		mStudentId = intent.getIntExtra("id", -1);
		mGroupId = intent.getIntExtra("group", -1);
		new ScheduleTask().execute(true);
	}
	
	private class ScheduleTask extends AsyncTask<Boolean, String, Boolean> {

		private ProgressDialog mProgress;
		private StudentInfo mStudentInfo;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			mProgress = new ProgressDialog(StudentInfoActivity.this);
			mProgress.setMessage("Getting Data ...");
			mProgress.setIndeterminate(false);
			mProgress.setCancelable(false);
			mProgress.show();
		}

		@Override
		protected Boolean doInBackground(Boolean... params) {
			boolean result = false;
			try {
				Boolean isCache = params[0];  
				mStudentInfo = mAttendanceAPI.getStudentInfo(mGroupId, mStudentId,isCache);
				if (mStudentInfo != null) {
					updateListSchedule(mStudentInfo.getAttendance());
					result = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			mProgress.dismiss();
			if (result) {
				// update view
				updateStudentInfo();
				mScheduleAdapter.notifyDataSetChanged();
			} else {
				// handle:something went wrong
			}
		}

		private void updateStudentInfo() {
			mTvName.setText(mStudentInfo.getStudent().getFirstName()+"  "+mStudentInfo.getStudent().getLastName());
			int absent = mStudentInfo.getStatistic().getAbsent();
			int late = mStudentInfo.getStatistic().getLate();
			int present = mStudentInfo.getStatistic().getPresent();
			String status = String.format(Locale.US,"Present:%d, Late:%d, Absent:%d", present, late, absent);
			String predefine = mStudentInfo.getStudent().getPredefinedId();
			
			if(mStudentInfo.getStudent().getGender() == User.GENDER_FEMALE){
				mIvSex.setImageResource(R.drawable.female);
			}else{
				mIvSex.setImageResource(R.drawable.male);
			}
			mTvPredefine.setText(predefine);
			mTvStat_present.setText(String.format(Locale.US,"Present:%d", present));
			mTvStat_late.setText(String.format(Locale.US,"Late:%d", late));
			mTvStat_absent.setText(String.format(Locale.US,"Absent:%d", absent));
			String urlImage = mStudentInfo.getStudent().getPhoto();
			Picasso.with(getApplicationContext()).load(urlImage)
			.error(R.drawable.ic_action_user)
			.into(mIvProfile);
		}
	}
	
	public void updateListSchedule(List<Attendance> mListSchedul) {
		if(mListSchedul != null && mListSchedul.size()>0) {
			mScheduleAdapter.updateSchedule(mListSchedul);
		}
	}
	
	@SuppressLint("SimpleDateFormat") class ScheduleAdapter extends BaseAdapter {
		private Context mContext;
		private List<Attendance> iListSchedul;
		private String colorStr;
		public ScheduleAdapter(Context context) {
			mContext = context;
			iListSchedul = new ArrayList<Attendance>();
		}

		public void updateSchedule(List<Attendance> listSchedul) {
			iListSchedul.clear();
			iListSchedul.addAll(listSchedul);
		}

		@Override
		public int getCount() {
			return iListSchedul.size();
		}

		@Override
		public Attendance getItem(int position) {
			return iListSchedul.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				//inflate item
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.item_lv_info,
						parent, false);
				//new holder
				holder = new ViewHolder();
				holder.tvSubject = (TextView) convertView
						.findViewById(R.id.tv_subject_code);
				//set holder in view
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Attendance attendance = iListSchedul.get(position);
			
			if(attendance != null){
				SimpleDateFormat date = new SimpleDateFormat("dd MMMM yyyy | HH:mm");
				String dateTime  =date.format(new Date(1000*attendance.getClazz().getDatetimeSeconds()));
				if(attendance.getCheckInStatus() == StudentAttendance.STATUS_ABSENCE){
					convertView.setBackgroundResource(R.drawable.balloon_red_info);
					colorStr = mContext.getResources().getString(
							R.color.red);
				}else if(attendance.getCheckInStatus() == StudentAttendance.STATUS_LATE){
					convertView.setBackgroundResource(R.drawable.balloon_yellow_info);
					colorStr = mContext.getResources().getString(
							R.color.yellow);
				}else if(attendance.getCheckInStatus() == StudentAttendance.STATUS_PRESENT){
					// check in already
					convertView.setBackgroundResource(R.drawable.balloon_blue);
					colorStr = mContext.getResources().getString(
							R.color.green);
				}else{
					convertView.setBackgroundResource(R.drawable.balloon_clean_na);
					colorStr = mContext.getResources().getString(
							android.R.color.black);
				}
				holder.tvSubject.setTextColor(Color.parseColor(colorStr));
				holder.tvSubject.setText(dateTime);
				
			}
			
			return convertView;
		}

	}

	class ViewHolder {
		public TextView tvSubject;
	}

	@Override
	public void doRefreshApi() {
		new ScheduleTask().execute(false);		
	}
}
