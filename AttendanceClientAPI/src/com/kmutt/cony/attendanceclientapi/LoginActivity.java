package com.kmutt.cony.attendanceclientapi;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kmutt.cony.api.AttendanceAPIZombie;

@SuppressLint("InflateParams") public class LoginActivity extends BaseActivity {

	AttendanceAPIZombie mAPIconnection;
	EditText mEtusername;
	EditText mEtpassword;
	Button login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		FrameLayout parentContent = (FrameLayout) findViewById(R.id.parent_content);
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.lb_title_login);
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View view = inflater.inflate(R.layout.activity_login, null);
		parentContent.addView(view);
		
		findViewById(R.id.bt_logout).setVisibility(View.INVISIBLE);
		findViewById(R.id.iv_refresh).setVisibility(View.INVISIBLE);
		

		mEtusername = (EditText) findViewById(R.id.edt_username);
		mEtpassword = (EditText) findViewById(R.id.edt_password);
		login = (Button) findViewById(R.id.bt_login);
		
		mAPIconnection = AttendanceAPIZombie.getInstance();
		String path = getApplicationContext().getFilesDir().getAbsolutePath();
		mAPIconnection.setPath(path);
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				//check internet before press
				boolean isConnect = isNetworkAvailable();
				if(isConnect){
					if (mEtusername.getText().toString().matches("")
							&& mEtpassword.getText().toString().matches("")) {
						Toast.makeText(LoginActivity.this,
								"You did not enter username and password",
								Toast.LENGTH_SHORT).show();
					} else if (mEtusername.getText().toString().matches("")) {
						Toast.makeText(LoginActivity.this,
								"You did not enter username", Toast.LENGTH_SHORT)
								.show();
					} else if (mEtpassword.getText().toString().matches("")) {
						Toast.makeText(LoginActivity.this,
								"You did not enter password", Toast.LENGTH_SHORT)
								.show();
					} else {
						new LoginTask().execute();
					}
				}else{
					String errorMsg = getString(R.string.error_internet);
					Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
				}
				//
				
			}
		});
		
		
		restoreUserLogin();
		
	}
	
	private void restoreUserLogin() {
		if(mAPIconnection.isLogin()){
			new RestoreLoginTask().execute();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class LoginTask extends AsyncTask<Void, String, String> {

		private ProgressDialog mProgress;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mAPIconnection.setCredentail(mEtusername.getText().toString(), mEtpassword
					.getText().toString());

			mProgress = new ProgressDialog(LoginActivity.this);
			mProgress.setMessage("Getting Data ...");
			mProgress.setIndeterminate(false);
			mProgress.setCancelable(true);
			mProgress.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {
				mAPIconnection.getMyInfo();
				return "0";
			} catch (Exception e) {
				return "1";
			}

		}

		@Override
		protected void onPostExecute(String result) {
			mProgress.dismiss();
			if (result.equals("1")) {
				Toast.makeText(LoginActivity.this,
						"Wrong username or password", Toast.LENGTH_SHORT)
						.show();
			} else {

				mEtusername.setText("");
				mEtpassword.setText("");

				startActivity(new Intent(getApplicationContext(),
						CourseListActivity.class));
				finish();
			}
		}
	}

	@Override
	public void doRefreshApi() {
		
	}

	private class RestoreLoginTask extends AsyncTask<Void, String, Boolean> {
		ProgressDialog mProgress;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgress = ProgressDialog.show(LoginActivity.this,getString(R.string.title_login_dialog),
					getString(R.string.msg_login_dialog));
			
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			boolean isRestore = false;
			try {
				
				 isRestore = mAPIconnection.restoreUser();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return isRestore;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgress.dismiss();
			if(result){
				startActivity(new Intent(getApplicationContext(),
						CourseListActivity.class));
				finish();
			}else{
				Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private Boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}
}
