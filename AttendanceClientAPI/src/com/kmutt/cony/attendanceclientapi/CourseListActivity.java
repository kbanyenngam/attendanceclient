package com.kmutt.cony.attendanceclientapi;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kmutt.cony.api.AttendanceAPIZombie;
import com.kmutt.cony.model.zombie.Course;
import com.kmutt.cony.model.zombie.Groups;
import com.kmutt.cony.model.zombie.User;

@SuppressLint("InflateParams")
public class CourseListActivity extends BaseActivity {

	private AttendanceAPIZombie APIconnection;
	// private ScheduleAdapter adapter;
	private List<Course> mListCourse;
	private ExpandableListView mListview;
	private ScheduleExpandAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FrameLayout parentContent = (FrameLayout) findViewById(R.id.parent_content);
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.lb_title_course);
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View view = inflater.inflate(R.layout.activity_course, null);
		parentContent.addView(view);

		mListview = (ExpandableListView) findViewById(R.id.elv_classroom);

		Drawable icon = this.getResources().getDrawable(
				R.drawable.arrow_expandable);
		Drawable line = this.getResources().getDrawable(
				R.drawable.info_line_horizontal);
		mListview.setGroupIndicator(icon);
		mListview.setChildDivider(line);
		adapter = new ScheduleExpandAdapter(this);
		mListview.setAdapter(adapter);

		APIconnection = AttendanceAPIZombie.getInstance();
		new ClassTask().execute(true);

		mListview.setOnChildClickListener(mOnChildClickListener);

	}

	private OnChildClickListener mOnChildClickListener = new OnChildClickListener() {

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			Intent intentSchedule = new Intent(getApplicationContext(),
					ClassScheduleListActivity.class);
			Course course = (Course) parent.getExpandableListAdapter().getGroup(groupPosition);
			
			Groups group = (Groups) parent.getExpandableListAdapter().getChild(
					groupPosition, childPosition);
			int groupId = group.getGroupId();
			intentSchedule.putExtra("group", groupId);
			intentSchedule.putExtra("courseName", course.getCourseName());
			startActivityForResult(intentSchedule,1);

			return false;
		}
	};

	private class ClassTask extends AsyncTask<Boolean, String, String> {

		private ProgressDialog mProgress;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mProgress = new ProgressDialog(CourseListActivity.this);
			mProgress.setMessage("Getting Data ...");
			mProgress.setIndeterminate(false);
			mProgress.setCancelable(false);
			mProgress.show();
		}

		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub

			try {
				
				mListCourse = APIconnection.getCourseList(params[0]);
				return "0";
			} catch (Exception e) {
				return "1";
			}

		}

		@Override
		protected void onPostExecute(String result) {
			mProgress.dismiss();
			if (result.equals("1")) {
				Toast.makeText(CourseListActivity.this,
						"Internet acess problem", Toast.LENGTH_SHORT).show();
			} else {
				adapter.updateSchedule(mListCourse);
				adapter.notifyDataSetChanged();
			}
		}
	}

	class ScheduleExpandAdapter extends BaseExpandableListAdapter {

		private Context mContext;
		private List<Course> iListGroups;

		public ScheduleExpandAdapter(Context context) {
			mContext = context;
			iListGroups = new ArrayList<Course>();
		}

		public void updateSchedule(List<Course> mListCourse) {
			iListGroups.clear();
			iListGroups.addAll(mListCourse);
		}

		@Override
		public int getGroupCount() {
			return iListGroups.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			int child = iListGroups.get(groupPosition).getGroups().size();
			return child;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return iListGroups.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return iListGroups.get(groupPosition).getGroups()
					.get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				// inflate item
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.item_lv_schedule,
						parent, false);
				// new holder
				holder = new ViewHolder();
				holder.tvSubject = (TextView) convertView
						.findViewById(R.id.tv_subject_code);
				// set holder in view
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Course groups = iListGroups.get(groupPosition);
			if (groups != null) {
				holder.tvSubject.setText("       " + groups.getCourseName());
			}

			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				// inflate item
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.item_lv_schedule_child,
						parent, false);
				// new holder
				holder = new ViewHolder();
				holder.tvSubject = (TextView) convertView
						.findViewById(R.id.tv_subject_code_child);
				// set holder in view
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Groups groups = iListGroups.get(groupPosition).getGroups()
					.get(childPosition);
			if (groups != null) {

				holder.tvSubject.setText(groups.getDescription());
			}
		
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

	}

	class ViewHolder {
		public TextView tvSubject;
	}

	@Override
	public void doRefreshApi() {
		new ClassTask().execute(false);
	}
}
