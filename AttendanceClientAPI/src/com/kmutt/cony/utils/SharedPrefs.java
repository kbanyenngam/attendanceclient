
package com.kmutt.cony.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPrefs {

    public static void setPreference(Context context, String key, String value) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putString(key, value);
        editor.commit();
        editor = null;
        app_preferences = null;
    }

    public static void setPreference(Context context, String key, boolean value) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
        editor = null;
        app_preferences = null;
    }

    public static void setPreference(Context context, String key, int value) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putInt(key, value);
        editor.commit();
        editor = null;
        app_preferences = null;
    }

    public static void setPreference(Context context, String key, long value) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putLong(key, value);
        editor.commit();
        editor = null;
        app_preferences = null;
    }

    public static void setPreference(Context context, String key, float value) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putFloat(key, value);
        editor.commit();
        editor = null;
        app_preferences = null;
    }

    public static String getPreferenceString(Context context, String key, String defValue) {
        String value = null;
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        value = app_preferences.getString(key, defValue);
        app_preferences = null;

        return value;
    }

    public static boolean getPreferenceBoolean(Context context, String key, boolean defValue) {
        boolean value = false;
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        value = app_preferences.getBoolean(key, defValue);
        app_preferences = null;

        return value;
    }

    public static int getPreferenceInt(Context context, String key, int defValue) {
        int value = 0;
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        value = app_preferences.getInt(key, defValue);
        app_preferences = null;

        return value;
    }

    public static long getPreferenceLong(Context context, String key, long defValue) {
        long value = 0;
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        value = app_preferences.getLong(key, defValue);
        app_preferences = null;

        return value;
    }

    public static float getPreferenceFloat(Context context, String key, float defValue) {
        float value = 0;
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        value = app_preferences.getFloat(key, defValue);
        app_preferences = null;

        return value;
    }
}
